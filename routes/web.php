<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/hotels/{id}', 'HotelsController@getHotelsByHotelier');
$router->get('/hotel/{id}', 'HotelsController@getHotel');
$router->get('/book-hotel/{id}', 'HotelsController@bookHotel');

$router->post('/hotels', 'HotelsController@storeHotel');
$router->put('/hotels', 'HotelsController@updateHotel');
$router->delete('/hotels', 'HotelsController@deleteHotel');