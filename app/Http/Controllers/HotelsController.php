<?php

namespace App\Http\Controllers;
use App\Hotel;
use App\Exceptions\Handler;
use App\Helpers\Validator;
use Illuminate\Http\Request;

use Crell\ApiProblem\ApiProblem;


class HotelsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * GET ALL HOTELS FROM THE TABLE BY HOTELIER ID
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $JsonResponse
     */
    public function getHotelsByHotelier(Request $request){
        
        $hotelsArr = array();
        
        //Get hotel by Hotelier ID
        $allHotels = Hotel::getAllHotels($request->id);

        //If result is not empty go trough all and fill the response array
        if(!empty($allHotels)){
            foreach($allHotels as $hotelKey => $hotelValue){
                $hotelsArr[$hotelValue->id] = self::returnHotelObjectFromEloquent($hotelValue);
            }
        }else{
            return Handler::handleError('not-found', $request);
        }

        return collect($hotelsArr);
    }

    /**
     * GET SINGLE HOTEL FROM THE TABLE BY ID
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $JsonResponse
     */
    public function getHotel(Request $request){
        
        
        //I USE ELOQUENT IN THIS CASE JUST TO SEE THE DIFFERENCE
        $singleHotel = Hotel::join('locations', 'hotels.location_id', '=', 'locations.id')
                        ->select(
                            'hotels.id', 
                            'hotels.name', 
                            'hotels.rating',
                            'hotels.category',
                            'hotels.image',
                            'hotels.reputation',
                            'hotels.reputation_badge',
                            'hotels.price',
                            'hotels.availability',
                            'locations.city',
                            'locations.state',
                            'locations.country',
                            'locations.zip_code',
                            'locations.address'
                            )
                        ->where("hotels.id", $request->id)->first();

        if($singleHotel == null){
            return Handler::handleError('not-found', $request);
        }

        $hotelObj[$singleHotel->id] = self::returnHotelObjectFromEloquent($singleHotel);

        return collect($hotelObj);
        
    }

    /**
     * STORE HOTEL IN DB
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $JsonResponse
     */
    public function storeHotel(Request $request){
        
        // validate post data
        $validate_request = Validator::validateInsertHotelForm($request);
        

        //if data doesn't pass validation trown an error
        if(!$validate_request){
            return Handler::handleError('hotel-validation-failed', $request);
        }
        

        $storeHotel = Hotel::storeTheHotel($request);
        return collect($storeHotel);
    }

    /**
     * UPDATE HOTEL IN DB
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $JsonResponse
     */
    public function updateHotel(Request $request){
        
        // validate post data
        $validate_request = Validator::validateInsertHotelForm($request);
        
        //if data doesn't pass validation trown an error
        if(!$validate_request){
            return Handler::handleError('hotel-validation-failed', $request);
        }

        //Get hotel by  ID
        $getTheHotel = Hotel::find($request->id);

        if($getTheHotel === null){
            return Handler::handleError('not-found', $request, "Hotel you want to update can't be found");
        }
        

        $updateHotel = Hotel::updateTheHotel($request);
        return collect($updateHotel);
    }

    /**
     * DELETE HOTEL FROM DB
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $JsonResponse
     */
    public function deleteHotel(Request $request){
        
        //Get hotel by  ID
        $getTheHotel = Hotel::find($request->id);

        if($getTheHotel === null){
            return Handler::handleError('not-found', $request, "Hotel you want to delete can't be found");
        }
        

        Hotel::deleteTheHotel($request);

        $response = [];
        $response['response'] = 'The hotel is deleted';

        return response()->json($response, 410);
    }

    /**
     * BOOK HOTEL BY HIS ID
     * WHENEVER THIS FUNCTION IS CALLED THE AVAILABILITY DROPS BY ONE
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $JsonResponse
     */
    public function bookHotel(Request $request){

        //Get the hotel
        $singleHotel = Hotel::find($request->id);

        //Check if hotel exists
        if(!$singleHotel){
            return Handler::handleError('not-found', $request);
        }

        //Check for availability
        if($singleHotel->availability <= 0){
            return Handler::handleError('hotel-not-available', $request);
        }

        //Calculate availability
        $new_availability = $singleHotel->availability - 1;
        
        

        //Try to book
        try {
            $singleHotel->availability = $new_availability;
            $singleHotel->save();

            $response = [];
            $response['response'] = 'Your hotel is booked';

            return response()->json($response);
        } catch (\Illuminate\Database\QueryException $e) {
            return Handler::handleError('internal-server-error', $request);
        }
    }

    /*
     * OBJECT OF HOTEL FOR RETURNING INTO FRONT 
     */
    private function returnHotelObjectFromEloquent($elObject) {
        $hotelsResponse = [];

        $hotelsResponse['name'] = $elObject->name;
        $hotelsResponse['rating'] = $elObject->rating;
        $hotelsResponse['category'] = $elObject->category;
        $hotelsResponse['location']['city'] = $elObject->city;
        $hotelsResponse['location']['state'] = $elObject->state;
        $hotelsResponse['location']['country'] = $elObject->country;
        $hotelsResponse['location']['zip_code'] = $elObject->zip_code;
        $hotelsResponse['location']['address'] = $elObject->address;
        $hotelsResponse['image'] = $elObject->image;
        $hotelsResponse['reputation'] = $elObject->reputation;
        $hotelsResponse['reputationBadge'] = $elObject->reputation_badge;
        $hotelsResponse['price'] = $elObject->price;
        $hotelsResponse['availability'] = $elObject->availability;

        return $hotelsResponse;

    }

    //
}
