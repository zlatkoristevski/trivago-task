<?php

namespace App\Helpers;


use App\Exceptions\Handler;
use App\Helpers\Validator;
use Illuminate\Http\Request;
use DB;

class Validator
{
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public static function validateInsertHotelForm($request){

        //check if id pass requirements
        if($request->id){
            if (!ctype_digit($request->id))
                return false;
        }
        
        //check if name pass requirements
        if($request->name){
            if (self::contains($request->name, ['offer', 'free', 'book', 'website'])) 
                return false;
        }else{
            return false;
        }

        //check if rating pass requirements
        if($request->rating){
            if (ctype_digit($request->rating) && intval($request->rating) >= 0 && intval($request->rating) <= 5) 
                $pass;
            else
                return false;
        }else{
            return false;
        }

        //check if category pass requirements
        if($request->category){
            if (!in_array($request->category, ['hotel', 'alternative', 'hostel', 'lodge', 'resort', 'guest-house']))
                return false;
        }else{
            return false;
        }

        //check if image pass requirements
        if($request->image){
            if (filter_var($request->image, FILTER_VALIDATE_URL) === FALSE)
                $validation_passed = false;
        }else{
            return false;
        }

        //check if reputation pass requirements
        if($request->reputation){
            if (ctype_digit($request->reputation) && intval($request->rating) >= 0 && intval($request->reputation) <= 1000) 
                $pass;
            else
                return false;
        }else{
            return false;
        }

        //check if price pass requirements
        if($request->price){
            if (!ctype_digit($request->price))
                return false;
        }else{
            return false;
        }

        //check if location_id pass requirements
        if($request->zip_code){
            if (!ctype_digit($request->zip_code) || strlen($request->zip_code) != 5)
                return false;
        }else{
            return false;
        }


        return true;
    }

    private static function contains($str, array $arr)
    {
        foreach($arr as $a) {
            if (stripos(strtolower($str),strtolower($a)) !== false) return true;
        }
        return false;
    }
}
