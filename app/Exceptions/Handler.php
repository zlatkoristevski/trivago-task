<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Crell\ApiProblem\ApiProblem;


class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function render($request, Exception $exception)
    {
        $problem = new ApiProblem("Unvalid URL.", $request->fullUrl());
        $problem
            ->setDetail("You accessed unvalid url")
            ->setInstance($request->fullUrl());

        return response()->json($problem, 401, ['X-Header-One' => 'Header Value']);
        
        return parent::render($request, $exception);
    }

    /**
     * Handle an error in HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $error_type
     * @return $JsonResponse
     */
    public static function handleError($error_type, $request, $error_problem_data = null, $error_details_data = null ){
        $error_code = null;
        $error_problem = null;
        $error_details = null;


        switch ($error_type) {
            case "not-found":
                $error_code = 404;
                $error_problem = $error_problem_data ? $error_problem_data : "Item Not Found";
                $error_details = $error_details_data ? $error_details_data : "The item you are looking for is not in the database";
                break;
            case "hotel-not-available": 
                $error_code = 406;
                $error_problem = $error_problem_data ? $error_problem_data : "Item Not Acceptable for Booking";
                $error_details = $error_details_data ? $error_details_data : "The item you are looking for currently unavailable for booking";
                break;
            case "hotel-validation-failed": 
                $error_code = 400;
                $error_problem = $error_problem_data ? $error_problem_data : "Post request didn't passed validation criteria";
                $error_details = $error_details_data ? $error_details_data : "
                A hotel name cannot contain the words [\"Free\", \"Offer\", \"Book\", \"Website\"] and it should be longer than 10 characters
                The rating MUST accept only integers, where rating is >= 0 and <= 5.
                The category can be one of [hotel, alternative, hostel, lodge, resort, guest-house] and it should be a string
                The location MUST be stored on a separate table.
                The zip code MUST be an integer and must have a length of 5.
                The image MUST be a valid URL
                The reputation MUST be an integer >= 0 and <= 1000.
                If reputation is <= 500 the value is red
                If reputation is <= 799 the value is yellow
                Otherwise the value is green
                The reputation badge is a calculated value that depends on the reputation
                The price must be an integer
                The availability must be an integer
                ";
                break;
            case "internal-server-error":
                $error_code = 500;
                $error_problem = $error_problem_data ? $error_problem_data : "Error on server";
                $error_details = $error_details_data ? $error_details_data : "Please contact your administrator";
                break;
            default:
                $error_problem = $error_problem_data ? $error_problem_data : "Something happened! Please contact your Admin";
                $error_details = $error_details_data ? $error_details_data : "An error occured. We are working hard to fix it!";
        }


        $problem = new ApiProblem($error_problem, $request->fullUrl());
        $problem
            ->setDetail($error_details)
            ->setInstance($request->fullUrl());
        

        return response()->json($problem, $error_code);
    }
}
