<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Exceptions\Handler;
use App\Location;
use DB;

class Hotel extends Model {
    protected $table = 'hotels';


    public static function getAllHotels($id){
        $allHotels = DB::select( 
            DB::raw(
                " 
                    SELECT h.*, l.city, l.state, l.country, l.zip_code, l.address
                    FROM hotels h
                    INNER JOIN locations l ON l.id = h.location_id
                    WHERE h.hotelier_id = :value
                "
            ) , array(
                'value' => $id,
            )
        );

        return $allHotels;
    }

    public static function storeTheHotel($request){
       
        //Try to insert
        try {
            //Save location
            $location = new Location;
            $location->city = $request->city;
            $location->state = $request->state;
            $location->country = $request->country;
            $location->zip_code = $request->zip_code;
            $location->address = $request->address;
            $location->save();


            //build reputation badge
            if($request->reputation >= 800){
                $badge = 'green';
            }else if($request->reputation >= 500){
                $badge = 'yellow';
            }else{
                $badge = 'red';
            }

            //Save hotel
            $hotel = new Hotel;
            $hotel->name = $request->name;
            $hotel->hotelier_id = $request->hotelier_id;
            $hotel->rating = $request->rating;
            $hotel->category = $request->category;
            $hotel->location_id = $location->id;
            $hotel->image = $request->image;
            $hotel->reputation = $request->reputation;
            $hotel->reputation_badge = $badge;
            $hotel->price = $request->price;
            $hotel->availability = $request->availability;
            $hotel->save();

            return $hotel;
        } catch (\Illuminate\Database\QueryException $e) {
            return Handler::handleError('internal-server-error', $request);
        }
    }

    public static function updateTheHotel($request){
        //Try to insert
        try {
            //build reputation badge
            if($request->reputation >= 800){
                $badge = 'green';
            }else if($request->reputation >= 500){
                $badge = 'yellow';
            }else{
                $badge = 'red';
            }

            //Update hotel
            $hotel = Hotel::find($request->id);
            $hotel->name = $request->name;
            $hotel->hotelier_id = $request->hotelier_id;
            $hotel->rating = $request->rating;
            $hotel->category = $request->category;
            $hotel->image = $request->image;
            $hotel->reputation = $request->reputation;
            $hotel->reputation_badge = $badge;
            $hotel->price = $request->price;
            $hotel->availability = $request->availability;
            $hotel->save();

            //Update location
            $location = Location::find($hotel->location_id);
            $location->city = $request->city;
            $location->state = $request->state;
            $location->country = $request->country;
            $location->zip_code = $request->zip_code;
            $location->address = $request->address;
            $location->save();

            return $hotel;
        } catch (\Illuminate\Database\QueryException $e) {
            return Handler::handleError('internal-server-error', $request);
        }
    }

    public static function deleteTheHotel($request){
        //Try to insert
        try {
            $hotel = Hotel::find($request->id);
            $hotel->delete();
        } catch (\Illuminate\Database\QueryException $e) {
            return Handler::handleError('internal-server-error', $request);
        }
    }
}