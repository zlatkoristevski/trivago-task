<?php

use App\Hotel;

class HotelsTest extends TestCase
{
    /**
     * /hotels/1 [GET]
     */
    public function testShouldReturnAllHotelsByHotelier(){
        $hotel = Hotel::first();


        $this->get("hotels/".$hotel->id, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            '*' => [
                'name', 
                'rating',
                'category',
                'location' => [
                    'city',
                    'state',
                    'country',
                    'zip_code',
                    'address',
                ],
                'image',
                'reputation',
                'reputationBadge',
                'price',
                'availability'
            ]
        ]);
    }

    /**
     * /hotel/1 [GET]
     */
    public function testShouldReturnSingleHotelsById(){
        $hotel = Hotel::first();

        $this->get("hotel/".$hotel->id, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            '*' => [
                'name', 
                'rating',
                'category',
                'location' => [
                    'city',
                    'state',
                    'country',
                    'zip_code',
                    'address',
                ],
                'image',
                'reputation',
                'reputationBadge',
                'price',
                'availability'
            ]
        ]);
    }

    /**
     * /book-hotel/1 [GET]
     */
    public function testShouldUpdateAvailabilityOfHotel(){
        $hotel = Hotel::first();

        $this->get("book-hotel/".$hotel->id, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'response'
        ]);
    }

    /**
     * /hotels [POST]
     */
    public function testShouldReturnHotelDataAfterInsert(){
        $parameters = [
            'name' => 'Test',
            'hotelier_id' => '1',
            'rating' => '5',
            'category' => 'hotel',
            'city' => 'Skopje',
            'state' => 'Macedonia',
            'country' => 'Macedonia',
            'zip_code' => '12345',
            'address' => 'st. Mavrovska',
            'image' => 'https://q-cf.bstatic.com/images/hotel/max1280x900/234/234890162.jpg',
            'reputation' => '600',
            'price' => '500',
            'availability' => '500',
        ];

        $this->post("hotels", $parameters);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            '*' => [
                'name', 
                'rating',
                'category',
                'location_id',
                'image',
                'reputation',
                'reputation_badge',
                'price',
                'availability'
            ]
        ]);
    }

    /**
     * /hotels [PUT]
     */
    public function testShouldReturnHotelDataAfterUpdate(){
        $parameters = [
            'id' => '1',
            'name' => 'Test',
            'hotelier_id' => '1',
            'rating' => '5',
            'category' => 'hotel',
            'city' => 'Skopje',
            'state' => 'Macedonia',
            'country' => 'Macedonia',
            'zip_code' => '12345',
            'address' => 'st. Mavrovska',
            'image' => 'https://q-cf.bstatic.com/images/hotel/max1280x900/234/234890162.jpg',
            'reputation' => '600',
            'price' => '500',
            'availability' => '500',
        ];

        $this->put("hotels", $parameters);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            '*' => [
                'name', 
                'rating',
                'category',
                'location_id',
                'image',
                'reputation',
                'reputation_badge',
                'price',
                'availability'
            ]
        ]);
    }

    /**
     * /hotels [DELETE]
     */
    public function testShouldDeleteHotel(){
        $hotel = Hotel::latest()->first();
        $parameters = [
            'id' => $hotel->id,
        ];
        
        $this->delete("hotels", $parameters, []);
        $this->seeStatusCode(410);
        $this->seeJsonStructure([
                'response',
        ]);
    }

}